Dexter is a script for burying Exherbo packages and repositories. It requires
ruby and the paludis ruby bindings.

To bury a package:

    dexter.rb --repository-dir directory package1::repo1 package2::repo2 ...

    When burying individual packages the ::repo spec and --repository-dir option
    are required. If --graveyard-dir is not given the graveyard file will be put
    in the current working directory. Package names are disambiguated, so giving
    the category is optional.

    The repository directory must be a directory containing your repositories
    in folders with same name as the ::repo spec. This is the where dexter will
    look for the commit id from the package's removal, so you must remove the
    package before running dexter. The commit id used will be taken from the
    last commit on the package's directory in the given repository.

    Example:

        dexter.rb --repository-dir ~/exherbo/repo/ \
                  --graveyard-dir ~/exherbo/repo/graveyard \
                  firefox::desktop gdm::gnome links::net


To bury an entire repository:

    dexter.rb repository/repo-name

    The category 'repository' must be given when burying repositories and you
    must have the repository configured to bury it. If the repository is in
    ::unavailable-unofficial then you must add the entry to the graveyard file
    for unavailable-unofficial, since dexter does not currently do this for
    you. Dexter will also bury all packages in the repository, so you will need
    to be sure that those packages do not already exist elsewhere and remove
    them before running dexter.

    Example:

        dexter.rb --graveyard-dir ~/exherbo/repos/graveyard/ repository/spoonb


See --help for available options and short option names. Please be sure to
double check dexter's work before pushing.
