#! /usr/bin/ruby
# vim: set sw=4 sts=4 et tw=80 :

# Copyright (c) 2011 Brett Witherspoon <spoonb@cdspooner.com>
# Based in part upon 'playboy.rb', which is:
#     Copyright (c) 2008 David Leverton <levertond@googlemail.com>
#
# This program is is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License, version
# 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA

require 'Paludis'
require 'getoptlong'
require 'strscan'

def error(msg)
    $stderr.puts "#$0: error - #{msg}"
    exit 1
end

def warn(msg)
    $stderr.puts "#$0: warning - #{msg}"
end

module Enumerable
    def group_by
        res  = []
        prev = nil
        each do |item|
            curr = yield item
            res << [] if res.empty? || curr != prev
            res.last << item
            prev = curr
        end
        res
    end
end

class Graveyard
    Position = Struct.new(:category, :package)

    def initialize(graves)
        @graves = StringScanner.new(graves)
    end

    def string
        @graves.string
    end

    def find_pos(qpn)
        if @graves.skip_until(/^#{qpn.category}+\/\s*\n/)
            cat_pos = @graves.pos
            until @graves.check(/^[-\w]+\/\s*\n/) or @graves.eos?
                if @graves.check(/\s+[-\w]+\/\s*\n/)
                    pkg_pos = @graves.pos
                    case sanitize_name(@graves.matched).casecmp sanitize_name(qpn.package)
                        when 1
                            break
                        when -1
                            @graves.skip(/.*\n/)
                            next
                        when 0
                            error "Package exists! Unable to add to an existing entry."
                    end
                else
                    @graves.skip(/.*\n/)
                    pkg_pos = @graves.pos
                    next
                end
            end
            pos = Position.new(cat_pos, pkg_pos)
        else
            until @graves.eos?
                if @graves.check(/^[-\w]+\/\s*\n/)
                    cat_pos = @graves.pos
                    case sanitize_name(@graves.matched).casecmp sanitize_name(qpn.category)
                        when 1
                            break
                        when -1
                            @graves.skip(/.*\n/)
                            next
                        when 0
                            error "Found catagory, but expected not to! Something went wrong."
                    end
                else
                    @graves.skip(/.*\n/)
                    cat_pos = @graves.pos
                    next
                end
            end
            pos = Position.new(cat_pos, nil)
        end
    end

    def sanitize_name(name)
        name.strip.delete("-").delete("/")
    end
end

class Victim
    attr_reader :qpn

    def initialize(env, spec)
        @env     = env
        @spec    = spec
        @qpn     = spec.package
        @repo    = nil
        @is_repo = false
        @is_pkg  = false
    end

    def repo
        @repo.nil? ? nil : @repo.name
    end

    def is_repo?
        @is_repo
    end

    def is_pkg?
        @is_pkg
    end

    protected

    def bury_slots(qpn)
        commit_id = ""
        homepage  = "[no homepage available]"
        desc      = "[no description available]"
        user_name = "#{`git config --get user.name`.strip} " + "<#{`git config --get user.email`.strip}>"
        grave     = ""

        if self.is_pkg?
            if File.directory?(@location + "/packages")
                Dir.chdir(@location + "/packages") unless Dir.pwd == @location + "/packages"
                commit_id = `git log -1 --pretty=%h -- #{qpn}`.strip
            end
            warn "Unable to get commit id from '#{@location}'" if commit_id.empty?
        end

        slots = @env[Paludis::Selection::AllVersionsGroupedBySlot.new(
                     Paludis::Generator::Package.new(qpn) &
                     Paludis::Generator::InRepository.new(@repo.name) |
                     Paludis::Filter::SupportsAction.new(Paludis::InstallAction))].
                group_by { |id| id.slot_key.parse_value }

        first_slot_in_pkg = true

        slots.each do |slot|
            if first_slot_in_pkg
                first_slot_in_pkg = false
                grave << "    #{qpn.package}/\n"
            end
            grave << "        :#{slot.last.slot_key.parse_value}"
            slot.each { |id| grave << " #{id.version}" }

            have_desc     = false
            have_homepage = false

            slot.reverse_each do |id|
                if !have_desc &&
                        !id.short_description_key.nil? &&
                        !id.short_description_key.parse_value.empty?
                    desc = id.short_description_key.parse_value
                    have_desc = true
                end
                if !have_homepage &&
                        !id.homepage_key.nil? &&
                        id.homepage_key.parse_value.any?
                    homepage = id.homepage_key.parse_value.first
                    have_homepage = true
                end
                if have_desc && have_homepage
                    break
                end
            end
            grave << "\n"
            grave << "            comment = #{@comment}\n" unless @comment.nil? or @comment.empty?
            grave << "            commit-id = #{commit_id}\n" if self.is_pkg?
            grave << "            description = #{desc}\n"
            grave << "            homepage = #{homepage}\n"
            grave << "            removed-by = #{user_name}\n"
            grave << "            removed-from = #{@repo.name}\n"
        end
        grave
    end
end

class Victim::Package < Victim
    def initialize(env, spec, repo_dir, comment=nil)
        super(env, spec)
        @repo     = env.fetch_repository(spec.in_repository)
        @comment  = comment
        @location = repo_dir + "/" + @repo.name
        @is_repo  = false
        @is_pkg   = true
    end

    def bury
        self.bury_slots(@qpn)
    end
end

class Victim::Repository < Victim
    def initialize(env, spec)
        super(env, spec)
        @repo    = env.fetch_repository(spec.package.package)
        @is_repo = true
        @is_pkg  = false
    end

    def bury
        graves = ""
        @repo.category_names do |cat|
            graves << "#{cat}/\n" unless @repo.package_names(cat).empty?
            @repo.package_names(cat) do |qpn|
                graves << self.bury_slots(qpn)
            end
        end
        graves
    end
end

Paludis::Log.instance.program_name = $0

options = GetoptLong.new(
            [ '--help',           '-h', GetoptLong::NO_ARGUMENT       ],
            [ '--log-level',      '-L', GetoptLong::REQUIRED_ARGUMENT ],
            [ '--environment',    '-E', GetoptLong::REQUIRED_ARGUMENT ],
            [ '--graveyard-dir',  '-g', GetoptLong::REQUIRED_ARGUMENT ],
            [ '--repository-dir', '-r', GetoptLong::REQUIRED_ARGUMENT ],
            [ '--comment',        '-c', GetoptLong::REQUIRED_ARGUMENT ])

graveyard_dir = Dir.pwd
repo_dir      = nil
targets       = []
comment       = nil
envspec       = ""

options.each do |opt, arg|
    case opt
        when '--help'
            puts "Usage: #$0 [OPTIONS] package1 package2 ..."
            puts
            puts "  --help, -h                  Display this help message"
            puts "  --log-level, -L             Paludis log level"
            puts "  --environment, -E           Paludis environment specification"
            puts "  --graveyard-dir, -g         Path to the graveyard repository"
            puts "  --repository-dir, -r        Path to a directory containing repositories"
            puts "  --comment, -c               A comment to include for all targets"
            puts
        exit 0

        when '--log-level'
            Paludis::Log.instance.log_level = case arg
                when 'silent' then  Paludis::LogLevel::Silent
                when 'warning' then Paludis::LogLevel::Warning
                when 'qa' then      Paludis::LogLevel::Qa
                when 'debug' then   Paludis::LogLevel::Debug
                else error "invalid #{opt}: #{arg}"
            end

        when '--environment'
            envspec = arg

        when '--graveyard-dir'
            graveyard_dir = arg.chomp("/")

        when "--repository-dir"
            repo_dir = arg.chomp("/")

        when "--comment"
            comment = arg

    end
end

error "No packages given. Try --help" if ARGV.empty?
error "#{graveyard_dir} does not exist or is not a directory" unless File.directory?(graveyard_dir)
error "#{repo_dir} does not exist or is not a directory" unless repo_dir.nil? or File.directory?(repo_dir)
error "#{graveyard_dir} is not writeable" unless File.writable?(graveyard_dir)

env = Paludis::EnvironmentFactory.instance.create(envspec)

ARGV.each do |arg|
    begin
        spec = Paludis::parse_user_package_dep_spec(arg, env, [:throw_if_set])
        if spec.package.category == "repository"
            targets << Victim::Repository.new(env, spec)
        else
            error "The --repository-dir option is required when burying packages." if repo_dir.nil?
            error "A repository spec must be given when burying packages." if spec.in_repository.nil?
            targets << Victim::Package.new(env, spec, repo_dir, comment)
        end
    rescue
        error $!
    end
end

targets.each do |target|
    filename = "#{graveyard_dir}/#{target.repo}.graveyard"
    puts "Writing to \'#{filename}\'"
    header   = "format = unwritten-1\n\n"
    if target.is_repo?
        unless File.exist?(filename)
            File.open(filename, "w") do |file|
                file.write(header)
                file.write(target.bury)
            end
        else
            error "Graveyard file \'#{filename}\' already exists! Refusing to overwrite."
        end
    end
    if target.is_pkg?
        if File.exist?(filename)
            File.open(filename, "r+") do |file|
                graveyard = Graveyard.new(file.read)
                file.seek(0)
                pos = graveyard.find_pos(target.qpn)
                if pos.package
                    file.print graveyard.string.insert(pos.package, target.bury)
                else
                    grave = "#{target.qpn.category}/\n" + target.bury
                    file.print graveyard.string.insert(pos.category, grave)
                end
            end
        else
            File.open(filename, "w") do |file|
                file.print header
                file.print "#{target.qpn.category}/\n"
                file.print target.bury
            end
        end
    end
end
